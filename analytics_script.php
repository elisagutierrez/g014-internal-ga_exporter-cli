<?php
setlocale(LC_TIME, 'es_ES');
date_default_timezone_set('europe/madrid');
$dateHour = date("Y-m-d h-s");
$dateServer = date("Y-M-d h:s");

echo "Iniciando Script Google Analytics\n";
echo "Fecha y hora del servidor: $dateServer \n";


/* --------------- Cargando funciones y libreria de Google API --------------- */
require_once('vendor/autoload.php');
require_once('analytics_functions.php');
$analytics = initializeAnalytics();
if($analytics){
	echo "Libreria Google API inicializada \n\n";		
}
else{
	echo "Error inicializando libreria Google API \n\n";
}
/* --------------- Cargando funciones y libreria de Google API --------------- */


/* --------------- Cargando archivo server.json --------------- */
$jsonServer = file_get_contents("server.json");
$server = json_decode($jsonServer);
/* --------------- Cargando archivo server.json --------------- */

/* --------------- Cargando archivo views.json --------------- */
$jsonViews = file_get_contents("views.json");
$analyticsViews = json_decode($jsonViews, true);
/* --------------- Cargando archivo views.json --------------- */

/* --------------- Cargando archivo correlative.json --------------- */
function nextCorrelative(){
	$jsonCorrelative = json_decode(file_get_contents('correlative.json'), true);
	return $jsonCorrelative;
}
/* --------------- Cargando archivo correlative.json --------------- */

/* --------------- Función para verificar creación de archivos y encabezados --------------- */
function ifCreateFile($fput, $fileName){
	if($fput){
		echo "Archivo $fileName creado con exito (Encabezados agregados)\n";	
	}
	else{
		echo "Error creando archivo $fileName\n";
	}
}
/* --------------- Función para verificar creación de archivos y encabezados --------------- */

/* --------------- Declarando arreglos a guardar en los csv --------------- */
$usersArray = [];
$sessionsArray = [];
$plantformDevicesArray = [];
$trafficSourcesArray = [];
/* --------------- Declarando arreglos a guardar en los csv --------------- */


/* --------------- Recorriendo arreglo de vistas de analytics --------------- */
foreach ($analyticsViews as $a => $b) {
   	$view = $b['view_id'];
   	$site = $b['site'];

   	echo "Consumiendo datos de la vista $site ($view) \n";

	$Users = getUsers($analytics, $view);
	$rowsUsers = $Users->reports[0]->data->rows;

	foreach($rowsUsers as $rowsUser){

		$ga_userType             = $rowsUser->dimensions[0]; 
		$ga_sessionCount         = $rowsUser->dimensions[1];
		$ga_daysSinceLastSession = $rowsUser->dimensions[2];
		$ga_userDefinedValue     = $rowsUser->dimensions[3];
		$ga_userBucket           = $rowsUser->dimensions[4];
		$ga_dateHourMinute       = $rowsUser->dimensions[5];

		$ga_users              = $rowsUser->metrics[0]->values[0];
		$ga_newUsers           = $rowsUser->metrics[0]->values[1];
		$ga_percentNewSessions = $rowsUser->metrics[0]->values[2];
		$ga_sessionsPerUser    = $rowsUser->metrics[0]->values[3];

		$date = date("Y-m-d", strtotime($ga_dateHourMinute));
		$strhour = $ga_dateHourMinute;
		$hour = $strhour[8] . $strhour[9] . ':' . $strhour[10] . $strhour[11];
        
        array_push($usersArray,array(
			$view, 
			$ga_users, 
			$ga_percentNewSessions, 
			$ga_sessionsPerUser, 
			$ga_userType, 
			$ga_sessionCount, 
			$ga_daysSinceLastSession,
			$ga_userDefinedValue,
			$ga_userBucket,
			$date,
			$hour
        ));
	}

	$Sessions = getSessions($analytics, $view);
	$rowsSessions = $Sessions->reports[0]->data->rows;

	foreach($rowsSessions as $rowsSession){
        $ga_sessionDurationBucket = $rowsSession->dimensions[0]; 
        $ga_dateHourMinute        = $rowsSession->dimensions[1];

        $ga_sessions                    = $rowsSession->metrics[0]->values[0];
        $ga_bounces                     = $rowsSession->metrics[0]->values[1]; 
        $ga_bounceRate                  = $rowsSession->metrics[0]->values[2];
        $ga_sessionDuration             = $rowsSession->metrics[0]->values[3];
        $ga_avgSessionDuration          = $rowsSession->metrics[0]->values[4];
        $ga_uniqueDimensionCombinations = $rowsSession->metrics[0]->values[5];
        $ga_hits                        = $rowsSession->metrics[0]->values[6];

       	$date = date("Y-m-d", strtotime($ga_dateHourMinute));
		$strhour = $ga_dateHourMinute;
		$hour = $strhour[8] . $strhour[9] . ':' . $strhour[10] . $strhour[11];

		array_push($sessionsArray, array(
			$view, 
			$ga_sessions, 
			$ga_sessionDurationBucket, 
			$ga_bounces, 
			$ga_bounceRate, 
			$ga_sessionDuration, 
			$ga_avgSessionDuration,
			$ga_uniqueDimensionCombinations,
			$ga_hits,
			$date,
			$hour
		)); 
	}


	$PlantformDevices = getPlatformDevices($analytics, $view);
	$rowsPlantformDevices = $PlantformDevices->reports[0]->data->rows;

	foreach($rowsPlantformDevices as $rowsPlantformDevice){

        $ga_browser                = $rowsPlantformDevice->dimensions[0];
        $ga_browserVersion         = $rowsPlantformDevice->dimensions[1];
        $ga_operatingSystem        = $rowsPlantformDevice->dimensions[2];
        $ga_operatingSystemVersion = $rowsPlantformDevice->dimensions[3];
        $ga_mobileDeviceBranding   = $rowsPlantformDevice->dimensions[4];
        $ga_mobileDeviceModel      = $rowsPlantformDevice->dimensions[5];
        $ga_mobileInputSelector    = $rowsPlantformDevice->dimensions[6];
        $ga_mobileDeviceInfo       = $rowsPlantformDevice->dimensions[7];
        $ga_dateHourMinute         = $rowsPlantformDevice->dimensions[8];

		$date = date("Y-m-d", strtotime($ga_dateHourMinute));
		$strhour = $ga_dateHourMinute;
		$hour = $strhour[8] . $strhour[9] . ':' . $strhour[10] . $strhour[11];

		array_push($plantformDevicesArray, array(
			$view, 
			$ga_browser, 
			$ga_browserVersion, 
			$ga_operatingSystem, 
			$ga_operatingSystemVersion, 
			$ga_mobileDeviceBranding, 
			$ga_mobileDeviceModel,
			$ga_mobileDeviceInfo,
			$ga_mobileInputSelector,
			$date,
			$hour
		));
	}	

	$TrafficSources = getTrafficSources($analytics, $view);
	$rowsTrafficSources = $TrafficSources->reports[0]->data->rows;

	foreach($rowsTrafficSources as $rowsTrafficSource){

        $ga_fullReferrer     = $rowsTrafficSource->dimensions[0];
        $ga_pageTitle        = $rowsTrafficSource->dimensions[1];
        $ga_pagePath         = $rowsTrafficSource->dimensions[2];
        $ga_exitPagePath     = $rowsTrafficSource->dimensions[3];
        $ga_previousPagePath = $rowsTrafficSource->dimensions[4];
        $ga_hostname         = $rowsTrafficSource->dimensions[5];
        $ga_dateHourMinute   = $rowsTrafficSource->dimensions[6];
 
        $ga_timeOnPage  = $rowsTrafficSource->metrics[0]->values[1]; 

		$date = date("Y-m-d", strtotime($ga_dateHourMinute));
		$strhour = $ga_dateHourMinute;
		$hour = $strhour[8] . $strhour[9] . ':' . $strhour[10] . $strhour[11];

		array_push($trafficSourcesArray, array(
			$view, 
			$ga_fullReferrer, 
			$ga_hostname, 
			$ga_pageTitle, 
			$ga_pagePath, 
			$ga_exitPagePath, 
			$ga_previousPagePath,
			$ga_timeOnPage,
			$date,
			$hour
		)); 
	}
}

##########################################################################################################
$usersCorrelative = nextCorrelative();
$usersCorrelative['next']++;
file_put_contents('correlative.json', json_encode($usersCorrelative));

$fileNameUsers = 'users-ga_analytics-'.$usersCorrelative['next'].'.csv';
$fileUsers = fopen($server->local_path . $fileNameUsers, 'w');
$fputUsers = fputcsv($fileUsers, array(
	'ganalytics', 
	'Users', 
	date(DateTime::ISO8601),
	$usersCorrelative['next'], 
	count($usersArray) + 2
));
$fputUsers = fputcsv($fileUsers, array(
	'application_id', 
	'ga_users', 
	'ga_percentNewSessions', 
	'ga_sessionsPerUser', 
	'ga_userType', 
	'ga_sessionCount', 
	'ga_daysSinceLastSession',
	'ga_userDefinedValue',
	'ga_userBucket',
	'date',
	'hour'
));
ifCreateFile($fputUsers, $fileNameUsers);


foreach ($usersArray as $userRow) {
    fputcsv($fileUsers, $userRow);
}
##########################################################################################################

##########################################################################################################
$sessionsCorrelative = nextCorrelative();
$sessionsCorrelative['next']++;
file_put_contents('correlative.json', json_encode($sessionsCorrelative));

$fileNameSessions = 'sessions-ga_analytics-'.$sessionsCorrelative['next'].'.csv';
$fileSessions = fopen($server->local_path . $fileNameSessions, 'w');
$fputSessions = fputcsv($fileSessions, array(
	'ganalytics', 
	'Sessions', 
	date(DateTime::ISO8601),
	$sessionsCorrelative['next'], 
	count($sessionsArray) + 2
));
$fputSessions = fputcsv($fileSessions, array(
	'application_id', 
	'ga_sessions', 
	'ga_sessionDurationBucket', 
	'ga_bounces', 
	'ga_bounceRate', 
	'ga_sessionDuration', 
	'ga_avgSessionDuration',
	'ga_uniqueDimensionCombinations',
	'ga_hits',
	'date',
	'hour'
)); 
ifCreateFile($fputSessions, $fileNameSessions);

foreach ($sessionsArray as $sessionRow) {
    fputcsv($fileSessions, $sessionRow);
}
##########################################################################################################

##########################################################################################################
$platfomDeviceCorrelative = nextCorrelative();
$platfomDeviceCorrelative['next']++;
file_put_contents('correlative.json', json_encode($platfomDeviceCorrelative));

$fileNamePlatformDevices = 'platform_device-ga_analytics-'.$platfomDeviceCorrelative['next'].'.csv';
$filePlatformDevices = fopen($server->local_path . $fileNamePlatformDevices, 'w');
$fputPlatformDevices = fputcsv($filePlatformDevices, array(
	'ganalytics', 
	'Platform or Device', 
	date(DateTime::ISO8601),
	$platfomDeviceCorrelative['next'], 
	count($plantformDevicesArray) + 2
));
$fputPlatformDevices = fputcsv($filePlatformDevices, array(
	'application_id', 
	'ga_browser', 
	'ga_browserVersion', 
	'ga_operatingSystem', 
	'ga_operatingSystemVersion', 
	'ga_mobileDeviceBranding', 
	'ga_mobileDeviceModel',
	'ga_mobileDeviceInfo',
	'ga_mobileInputSelector',
	'date',
	'hour'
)); 
ifCreateFile($fputPlatformDevices, $fileNamePlatformDevices);

foreach ($plantformDevicesArray as $plantformDeviceRow) {
    fputcsv($filePlatformDevices, $plantformDeviceRow);
}
##########################################################################################################

##########################################################################################################
$trafficSourceCorrelative = nextCorrelative();
$trafficSourceCorrelative['next']++;
file_put_contents('correlative.json', json_encode($trafficSourceCorrelative));

$fileNameTrafficSources = 'traffic_source-ga_analytics-'.$trafficSourceCorrelative['next'].'.csv';
$fileTrafficSources = fopen($server->local_path . $fileNameTrafficSources, 'w');
$fputTrafficSources = fputcsv($fileTrafficSources, array(
	'ganalytics', 
	'Traffic Source', 
	date(DateTime::ISO8601),
	$trafficSourceCorrelative['next'], 
	count($trafficSourcesArray) + 2
));
$fputTrafficSources = fputcsv($fileTrafficSources, array(
	'application_id', 
	'ga_fullReferrer', 
	'ga_hostname', 
	'ga_pageTitle', 
	'ga_pagePath', 
	'ga_exitPagePath', 
	'ga_previousPagePath',
	'ga_timeOnPage',
	'date',
	'hour'
)); 
ifCreateFile($fputTrafficSources, $fileNameTrafficSources);

foreach ($trafficSourcesArray as $trafficSourceRow) {
    fputcsv($fileTrafficSources, $trafficSourceRow);
}
##########################################################################################################

echo "\n";

fclose($fileUsers);
fclose($fileSessions);
fclose($filePlatformDevices);
fclose($fileTrafficSources);

//Función para subir archivos a servidor FTP
function uploadFile($FTPConnect, $fileCopy, $filePaste){

	$upload = ftp_put($FTPConnect, $filePaste, $fileCopy, FTP_BINARY);
	if (!$upload) {
		echo "Error copiando archivo $filePaste al servidor FTP\n";
	} 
	else {
		echo "Archivo $filePaste copiado al servidor FTP\n";
	}
}

//FTP Conexión
$FTPConnect = ftp_connect($server->ftp_host);
if($FTPConnect){

	echo "\nConexión exitosa a servidor FTP $server->ftp_host\n";

	//FTP Login
	$FTPLogin   = ftp_login($FTPConnect, $server->ftp_user, $server->ftp_password);
	if($FTPLogin){
		echo "Inicio de sesión exitoso con usuario $server->ftp_user \n";

		//FTP en modo pasivo
		$FTPpasv = ftp_pasv($FTPConnect, true);	
		if($FTPpasv){
			echo "Conexión FTP en modo pasivo\n";		
		}
		else{
			echo "Error conectando modo pasivo\n";
		}

		//Copiar Archivos CSV
		$usersCopy  = $server->local_path . $fileNameUsers;
		$usersPaste = $server->ftp_remote_path . $fileNameUsers;
		uploadFile($FTPConnect, $usersCopy, $usersPaste);

		$sessionsCopy  = $server->local_path . $fileNameSessions;
		$sessionsPaste = $server->ftp_remote_path . $fileNameSessions;
		uploadFile($FTPConnect, $sessionsCopy, $sessionsPaste);

		$platformDevicesCopy  = $server->local_path . $fileNamePlatformDevices;
		$platformDevicesPaste = $server->ftp_remote_path . $fileNamePlatformDevices;
		uploadFile($FTPConnect, $platformDevicesCopy, $platformDevicesPaste);

		$trafficSourcesCopy  = $server->local_path . $fileNameTrafficSources;
		$trafficSourcesPaste = $server->ftp_remote_path . $fileNameTrafficSources;
		uploadFile($FTPConnect, $trafficSourcesCopy, $trafficSourcesPaste);
	}
	else{
		echo "Error iniciando sesión con usuario $server->ftp_user \n";
	}
}
else{
	echo "\nError al conectar al servidor FTP $server->ftp_host \n";	
}

ftp_close($FTPConnect);
?>