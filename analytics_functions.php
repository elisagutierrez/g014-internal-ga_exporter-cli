<?php
/**
 * Initializes an Analytics Reporting API V4 service object.
 *
 * @return An authorized Analytics Reporting API V4 service object.
 */
function initializeAnalytics()
{

  // Use the developers console and download your service account
  // credentials in JSON format. Place them in this directory or
  // change the key file location if necessary.
  $KEY_FILE_LOCATION = __DIR__ . '/service-account-credentials.json';

  // Create and configure a new client object.
  $client = new Google_Client();
  $client->setApplicationName("Analytics Reporting");
  $client->setAuthConfig($KEY_FILE_LOCATION);
  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  $analytics = new Google_Service_AnalyticsReporting($client);

  return $analytics;
}

function getUsers($analytics, $view){
    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("yesterday");
    $dateRange->setEndDate("yesterday");


    // Metrics
    $users = new Google_Service_AnalyticsReporting_Metric();
    $users->setExpression("ga:users");
    $users->setAlias("users");

    $newUsers = new Google_Service_AnalyticsReporting_Metric();
    $newUsers->setExpression("ga:newUsers");
    $newUsers->setAlias("newUsers");

    $percentNewSessions = new Google_Service_AnalyticsReporting_Metric();
    $percentNewSessions->setExpression("ga:percentNewSessions");
    $percentNewSessions->setAlias("percentNewSessions");

    $sessionsPerUser = new Google_Service_AnalyticsReporting_Metric();
    $sessionsPerUser->setExpression("ga:sessionsPerUser");
    $sessionsPerUser->setAlias("sessionsPerUser");

    // Dimensions
    $userType = new Google_Service_AnalyticsReporting_Dimension();
    $userType->setName("ga:userType");

    $sessionCount = new Google_Service_AnalyticsReporting_Dimension();
    $sessionCount->setName("ga:sessionCount");

    $daysSinceLastSession = new Google_Service_AnalyticsReporting_Dimension();
    $daysSinceLastSession->setName("ga:daysSinceLastSession");

    $userDefinedValue = new Google_Service_AnalyticsReporting_Dimension();
    $userDefinedValue->setName("ga:userDefinedValue");

    $userBucket = new Google_Service_AnalyticsReporting_Dimension();
    $userBucket->setName("ga:userBucket");

    $dateHourMinute = new Google_Service_AnalyticsReporting_Dimension();
    $dateHourMinute->setName("ga:dateHourMinute");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($view);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array(
        $userType,
        $sessionCount,
        $daysSinceLastSession,
        $userDefinedValue,
        $userBucket,
        $dateHourMinute
    ));
    $request->setMetrics(array(
        $users,
        $newUsers,
        $percentNewSessions,
        $sessionsPerUser
    ));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}

function getSessions($analytics, $view){
    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("yesterday");
    $dateRange->setEndDate("yesterday");

    // Metrics
    $sessions = new Google_Service_AnalyticsReporting_Metric();
    $sessions->setExpression("ga:sessions");
    $sessions->setAlias("sessions");

    $bounces = new Google_Service_AnalyticsReporting_Metric();
    $bounces->setExpression("ga:bounces");
    $bounces->setAlias("bounces");

    $bounceRate = new Google_Service_AnalyticsReporting_Metric();
    $bounceRate->setExpression("ga:bounceRate");
    $bounceRate->setAlias("bounceRate");

    $sessionDuration = new Google_Service_AnalyticsReporting_Metric();
    $sessionDuration->setExpression("ga:sessionDuration");
    $sessionDuration->setAlias("sessionDuration");

    $avgSessionDuration = new Google_Service_AnalyticsReporting_Metric();
    $avgSessionDuration->setExpression("ga:avgSessionDuration");
    $avgSessionDuration->setAlias("avgSessionDuration");

    $uniqueDimensionCombinations = new Google_Service_AnalyticsReporting_Metric();
    $uniqueDimensionCombinations->setExpression("ga:uniqueDimensionCombinations");
    $uniqueDimensionCombinations->setAlias("uniqueDimensionCombinations");

    $hits = new Google_Service_AnalyticsReporting_Metric();
    $hits->setExpression("ga:hits");
    $hits->setAlias("hits");

    // Dimensions
    $sessionDurationBucket = new Google_Service_AnalyticsReporting_Dimension();
    $sessionDurationBucket->setName("ga:sessionDurationBucket");

    $dateHourMinute = new Google_Service_AnalyticsReporting_Dimension();
    $dateHourMinute->setName("ga:dateHourMinute");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($view);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array(
        $sessionDurationBucket,
        $dateHourMinute,
    ));
    $request->setMetrics(array(
        $sessions,
        $bounces,
        $bounceRate,
        $sessionDuration,
        $avgSessionDuration,
        $uniqueDimensionCombinations,
        $hits
    ));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}

function getPlatformDevices($analytics, $view){
    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("yesterday");
    $dateRange->setEndDate("yesterday");

    // Metrics
    $users = new Google_Service_AnalyticsReporting_Metric();
    $users->setExpression("ga:users");
    $users->setAlias("users");

    //Dimensions
    $browser = new Google_Service_AnalyticsReporting_Dimension();
    $browser->setName("ga:browser");

    $browserVersion = new Google_Service_AnalyticsReporting_Dimension();
    $browserVersion->setName("ga:browserVersion");

    $operatingSystem = new Google_Service_AnalyticsReporting_Dimension();
    $operatingSystem->setName("ga:operatingSystem");

    $operatingSystemVersion = new Google_Service_AnalyticsReporting_Dimension();
    $operatingSystemVersion->setName("ga:operatingSystemVersion");

    $mobileDeviceBranding = new Google_Service_AnalyticsReporting_Dimension();
    $mobileDeviceBranding->setName("ga:mobileDeviceBranding");

    $mobileDeviceModel = new Google_Service_AnalyticsReporting_Dimension();
    $mobileDeviceModel->setName("ga:mobileDeviceModel");

    $mobileInputSelector = new Google_Service_AnalyticsReporting_Dimension();
    $mobileInputSelector->setName("ga:mobileInputSelector");

    $mobileDeviceInfo = new Google_Service_AnalyticsReporting_Dimension();
    $mobileDeviceInfo->setName("ga:mobileDeviceInfo");

    $dateHourMinute = new Google_Service_AnalyticsReporting_Dimension();
    $dateHourMinute->setName("ga:dateHourMinute");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($view);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array(
        $browser,
        $browserVersion,
        $operatingSystem,
        $operatingSystemVersion,
        $mobileDeviceBranding,
        $mobileDeviceModel,
        $mobileInputSelector,
        $mobileDeviceInfo,
        $dateHourMinute,
    ));
    $request->setMetrics(array(
        $users,
    ));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}

function getTrafficSources($analytics, $view){
    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("yesterday");
    $dateRange->setEndDate("yesterday");

    // Metrics
    $users = new Google_Service_AnalyticsReporting_Metric();
    $users->setExpression("ga:users");
    $users->setAlias("users");

    $timeOnPage = new Google_Service_AnalyticsReporting_Metric();
    $timeOnPage->setExpression("ga:timeOnPage");
    $timeOnPage->setAlias("timeOnPage");

    // Dimensions
    $fullReferrer = new Google_Service_AnalyticsReporting_Dimension();
    $fullReferrer->setName("ga:fullReferrer");

    $pageTitle = new Google_Service_AnalyticsReporting_Dimension();
    $pageTitle->setName("ga:pageTitle");

    $pagePath = new Google_Service_AnalyticsReporting_Dimension();
    $pagePath->setName("ga:pagePath");

    $exitPagePath = new Google_Service_AnalyticsReporting_Dimension();
    $exitPagePath->setName("ga:exitPagePath");

    $previousPagePath = new Google_Service_AnalyticsReporting_Dimension();
    $previousPagePath->setName("ga:previousPagePath");

    $hostname = new Google_Service_AnalyticsReporting_Dimension();
    $hostname->setName("ga:hostname");

    $dateHourMinute = new Google_Service_AnalyticsReporting_Dimension();
    $dateHourMinute->setName("ga:dateHourMinute");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($view);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array(
        $fullReferrer,
        $pageTitle,
        $pagePath,
        $exitPagePath,
        $previousPagePath,
        $hostname,
        $dateHourMinute
    ));
    $request->setMetrics(array(
        $users,
        $timeOnPage
    ));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}
?>
